create table provincias(
    PRO_ID INT UNSIGNED NOT NULL AUTO_INCREMENT,
    PRO_DESCRIPCION VARCHAR(45) NOT NULL,
    PRIMARY KEY(PRO_ID)
); 
 

create table partidos(
    PAR_ID int unsigned not null auto_increment,
    PRO_ID int unsigned not null,
    PAR_DESCRIPCION varchar(45) not null, 
    primary key (PAR_ID),
    CONSTRAINT FK_partidos_provincia FOREIGN KEY FK_partidos_provincia (PRO_ID)
        references provincias (PRO_ID)
        on delete restrict
        on update restrict
); 

insert into provincias(PRO_DESCRIPCION) values ('Buenos Aires');
insert into provincias(PRO_DESCRIPCION) values ('Santa Cruz');
insert into provincias(PRO_DESCRIPCION) values ('San Luis');
insert into provincias(PRO_DESCRIPCION) values ('Neuquén');

insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(1, 'Merlo');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(1, 'Moreno');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(1, 'Lomas de Zamora');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(1, 'Olavarría');

insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(2, 'Los Antiguos');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(2, 'El Chaltén');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(2, 'Puerto Deseado');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(2, 'Gobernador Gregores');

insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(3, 'Saladillo');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(3, 'Buena Esperanza');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(3, 'Leandro N. Alem');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(3, 'Quines');

insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(4, 'Zapala');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(4, 'Loncopué');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(4, 'Centenario');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(4, 'Vista Alegre');


describe partidos;
DESCRIBE provincias; 

select * from provincias 
select * from partidos

delete from provincias 
where PRO_ID=1

delete from provincias 
where PRO_ID=5

delete from partidos
where PRO_ID=4
delete from provincias
where PRO_ID=4

update provincias set PRO_DESCRIPCION='bsas' where PRO_ID=1

update provincias set PRO_DESCRIPCION='buenos mates' where PRO_ID=3

update partidos set PAR_DESCRIPCION='el mejor' where PAR_ID=1

update provincias set PRO_DESCRIPCION='cba' where PRO_ID=2

